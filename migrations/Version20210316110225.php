<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316110225 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE home (id INT AUTO_INCREMENT NOT NULL, titre1 VARCHAR(255) NOT NULL, titre2 VARCHAR(255) NOT NULL, titre3 VARCHAR(255) NOT NULL, titre4 VARCHAR(255) NOT NULL, titre VARCHAR(255) NOT NULL, ct1 VARCHAR(255) NOT NULL, ct2 VARCHAR(255) NOT NULL, ct3 VARCHAR(255) NOT NULL, ct4 VARCHAR(255) NOT NULL, ci1 VARCHAR(100) NOT NULL, ci2 VARCHAR(100) NOT NULL, ci3 VARCHAR(100) NOT NULL, ci4 VARCHAR(100) NOT NULL, ft1 VARCHAR(255) NOT NULL, ft2 VARCHAR(255) NOT NULL, ft3 VARCHAR(255) NOT NULL, ft4 VARCHAR(255) NOT NULL, fi1 VARCHAR(100) NOT NULL, fi2 VARCHAR(100) NOT NULL, fi3 VARCHAR(100) NOT NULL, fi4 VARCHAR(100) NOT NULL, clubt1 VARCHAR(255) NOT NULL, clubt2 VARCHAR(255) NOT NULL, clubt3 VARCHAR(255) NOT NULL, clubt4 VARCHAR(255) NOT NULL, clubi1 VARCHAR(100) NOT NULL, clubi2 VARCHAR(100) NOT NULL, clubi3 VARCHAR(100) NOT NULL, clubi4 VARCHAR(100) NOT NULL, active TINYINT(1) NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE home');
    }
}
