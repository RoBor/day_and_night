<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /** La méthode du controller prend en charge deux routes grâce aux annotations.
     * @Route("/", name="index")
     * @Route("/home", name="home")
     */
    public function index(): Response
    {
        // Le controller renvoie la vue d'un fichier nom.html.twig et lui passe accessoirement un tableau contenant les données à afficher.
        // Le rendu est "inclus" dans le template global nommé base.html.twig 
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
