<?php

namespace App\Entity;

use App\Repository\HomeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=HomeRepository::class)
 * @Vich\Uploadable
 */
class Home
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre3;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre4;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ct1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ct2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ct3;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ct4;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ci1;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="ci1")
     * 
     * @var File|null
     */
    private $ci1File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ci2;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="ci2")
     * 
     * @var File|null
     */
    private $ci2File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ci3;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="ci3")
     * 
     * @var File|null
     */
    private $ci3File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ci4;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="ci4")
     * 
     * @var File|null
     */
    private $ci4File;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ft1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ft2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ft3;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ft4;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $fi1;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="fi1")
     * 
     * @var File|null
     */
    private $fi1File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $fi2;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="fi2")
     * 
     * @var File|null
     */
    private $fi2File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $fi3;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="fi3")
     * 
     * @var File|null
     */
    private $fi3File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $fi4;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="fi4")
     * 
     * @var File|null
     */
    private $fi4File;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clubt1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clubt2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clubt3;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clubt4;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $clubi1;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="clubi1")
     * 
     * @var File|null
     */
    private $clubi1File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $clubi2;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="clubi2")
     * 
     * @var File|null
     */
    private $clubi2File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $clubi3;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="clubi3")
     * 
     * @var File|null
     */
    private $clubi3File;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $clubi4;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="image_upload", fileNameProperty="clubi4")
     * 
     * @var File|null
     */
    private $clubi4File;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __toString(){
        return "Home";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTitre1(): ?string
    {
        return $this->titre1;
    }

    public function setTitre1(string $titre1): self
    {
        $this->titre1 = $titre1;

        return $this;
    }

    public function getTitre2(): ?string
    {
        return $this->titre2;
    }

    public function setTitre2(string $titre2): self
    {
        $this->titre2 = $titre2;

        return $this;
    }

    public function getTitre3(): ?string
    {
        return $this->titre3;
    }

    public function setTitre3(string $titre3): self
    {
        $this->titre3 = $titre3;

        return $this;
    }

    public function getTitre4(): ?string
    {
        return $this->titre4;
    }

    public function setTitre4(string $titre4): self
    {
        $this->titre4 = $titre4;

        return $this;
    }

    public function getCt1(): ?string
    {
        return $this->ct1;
    }

    public function setCt1(string $ct1): self
    {
        $this->ct1 = $ct1;

        return $this;
    }

    public function getCt2(): ?string
    {
        return $this->ct2;
    }

    public function setCt2(string $ct2): self
    {
        $this->ct2 = $ct2;

        return $this;
    }

    public function getCt3(): ?string
    {
        return $this->ct3;
    }

    public function setCt3(string $ct3): self
    {
        $this->ct3 = $ct3;

        return $this;
    }

    public function getCt4(): ?string
    {
        return $this->ct4;
    }

    public function setCt4(string $ct4): self
    {
        $this->ct4 = $ct4;

        return $this;
    }

    public function getCi1(): ?string
    {
        return $this->ci1;
    }

    public function setCi1($ci1): self
    {
        $this->ci1 = $ci1;

        return $this;
    }

    public function setCi1File(?File $imageFile = null): void
    {
        $this->ci1File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getCi1File(): ?File
    {
        return $this->ci1File;
    }

    public function getCi2(): ?string
    {
        return $this->ci2;
    }

    public function setCi2($ci2): self
    {
        $this->ci2 = $ci2;

        return $this;
    }

    public function setCi2File(?File $imageFile = null): void
    {
        $this->ci2File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getCi2File(): ?File
    {
        return $this->ci2File;
    }

    public function getCi3(): ?string
    {
        return $this->ci3;
    }

    public function setCi3($ci3): self
    {
        $this->ci3 = $ci3;

        return $this;
    }

    public function setCi3File(?File $imageFile = null): void
    {
        $this->ci3File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getCi3File(): ?File
    {
        return $this->ci3File;
    }

    public function getCi4(): ?string
    {
        return $this->ci4;
    }

    public function setCi4($ci4): self
    {
        $this->ci4 = $ci4;

        return $this;
    }

    public function setCi4File(?File $imageFile = null): void
    {
        $this->ci4File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getCi4File(): ?File
    {
        return $this->ci4File;
    }

    public function getFt1(): ?string
    {
        return $this->ft1;
    }

    public function setFt1(string $ft1): self
    {
        $this->ft1 = $ft1;

        return $this;
    }

    public function getFt2(): ?string
    {
        return $this->ft2;
    }

    public function setFt2(string $ft2): self
    {
        $this->ft2 = $ft2;

        return $this;
    }

    public function getFt3(): ?string
    {
        return $this->ft3;
    }

    public function setFt3(string $ft3): self
    {
        $this->ft3 = $ft3;

        return $this;
    }

    public function getFt4(): ?string
    {
        return $this->ft4;
    }

    public function setFt4(string $ft4): self
    {
        $this->ft4 = $ft4;

        return $this;
    }

    public function getFi1(): ?string
    {
        return $this->fi1;
    }

    public function setFi1($fi1): self
    {
        $this->fi1 = $fi1;

        return $this;
    }

    public function setFi1File(?File $imageFile = null): void
    {
        $this->fi1File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getFi1File(): ?File
    {
        return $this->fi1File;
    }

    public function getFi2(): ?string
    {
        return $this->fi2;
    }

    public function setFi2($fi2): self
    {
        $this->fi2 = $fi2;

        return $this;
    }

    public function setFi2File(?File $imageFile = null): void
    {
        $this->fi2File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getFi2File(): ?File
    {
        return $this->fi2File;
    }

    public function getFi3(): ?string
    {
        return $this->fi3;
    }

    public function setFi3($fi3): self
    {
        $this->fi3 = $fi3;

        return $this;
    }

    public function setFi3File(?File $imageFile = null): void
    {
        $this->fi3File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getFi3File(): ?File
    {
        return $this->fi3File;
    }

    public function getFi4(): ?string
    {
        return $this->fi4;
    }

    public function setFi4($fi4): self
    {
        $this->fi4 = $fi4;

        return $this;
    }

    public function setFi4File(?File $imageFile = null): void
    {
        $this->fi4File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getFi4File(): ?File
    {
        return $this->fi4File;
    }

    public function getClubt1(): ?string
    {
        return $this->clubt1;
    }

    public function setClubt1(string $clubt1): self
    {
        $this->clubt1 = $clubt1;

        return $this;
    }

    public function getClubt2(): ?string
    {
        return $this->clubt2;
    }

    public function setClubt2(string $clubt2): self
    {
        $this->clubt2 = $clubt2;

        return $this;
    }

    public function getClubt3(): ?string
    {
        return $this->clubt3;
    }

    public function setClubt3(string $clubt3): self
    {
        $this->clubt3 = $clubt3;

        return $this;
    }

    public function getClubt4(): ?string
    {
        return $this->clubt4;
    }

    public function setClubt4(string $clubt4): self
    {
        $this->clubt4 = $clubt4;

        return $this;
    }

    public function getClubi1(): ?string
    {
        return $this->clubi1;
    }

    public function setClubi1($clubi1): self
    {
        $this->clubi1 = $clubi1;

        return $this;
    }

    public function setClubi1File(?File $imageFile = null): void
    {
        $this->clubi1File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getClubi1File(): ?File
    {
        return $this->clubi1File;
    }

    public function getClubi2(): ?string
    {
        return $this->clubi2;
    }

    public function setClubi2($clubi2): self
    {
        $this->clubi2 = $clubi2;

        return $this;
    }

    public function setClubi2File(?File $imageFile = null): void
    {
        $this->clubi2File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getClubi2File(): ?File
    {
        return $this->clubi2File;
    }

    public function getClubi3(): ?string
    {
        return $this->clubi3;
    }

    public function setClubi3($clubi3): self
    {
        $this->clubi3 = $clubi3;

        return $this;
    }

    public function setClubi3File(?File $imageFile = null): void
    {
        $this->clubi3File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getClubi3File(): ?File
    {
        return $this->clubi3File;
    }

    public function getClubi4(): ?string
    {
        return $this->clubi4;
    }

    public function setClubi4($clubi4): self
    {
        $this->clubi4 = $clubi4;

        return $this;
    }

    public function setClubi4File(?File $imageFile = null): void
    {
        $this->clubi4File = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getClubi4File(): ?File
    {
        return $this->clubi4File;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    
}
